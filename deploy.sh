#! bin/bash

if [[`uname` == 'MINGW64_NT-10.0']]; then
    cd e:/idle/
    npm run-script build
    cd e:/harrisonschultz.github.io
else
    cd ~/Documents/dev/idle
    npm run-script build
    cd ~/Documents/dev/harrisonschultz.github.io
fi
rm -rf idle
mkdir idle
cp -r ../idle/build/* idle/
git pull origin master
git add *
git commit -m "new idle build"
git push origin master
