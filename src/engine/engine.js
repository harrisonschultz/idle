// import BuildingAction from "../actions/buildingAction";
// import PopulationAction from "../actions/populationAction";
import { updateResource } from "../actions/resourceAction";
import RateAction from "../actions/rateAction";
import BuildingStore from "../stores/buildingStore";
import PopulationStore from "../stores/populationStore";
import ActionStore from "../stores/actionStore";
import TechStore from "../stores/techStore";
import ResourceStore from "../stores/resourceStore";
import UpgradeStore from "../stores/upgradeStore";
import RateStore from "../stores/rateStore";
import jobs from "../config/jobs.json";
import {log} from '../actions/logAction'

let resources = {};
let rates = {};
let population = {};
let intervals = {};

export default class Engine {
    constructor() {
        resources = ResourceStore.getAll();
        rates = RateStore.getAll();
        population = PopulationStore.getAll();
        intervals = {};

        this.addAllResourceListeners();
        this.addPopulationListner();

        //Load rates
        for(var key in resources) {
            this.updateRate(key);
        }

        //start saving game every 30 seconds
        this.saveEvery30();
        this.startRandomEvents();
    }

    startRandomEvents = () => {
        setInterval(() => {
            // log('random event');
        },5000)
    }

    addAllResourceListeners() {
        for(var key in resources) {
            ResourceStore.addChangeListener(this.onResourceChange, key);
        }
    }

    addPopulationListner() {
        PopulationStore.addChangeListener(this.onPopulationChange);
    }

    saveEvery30() {
        setInterval( () => {
            BuildingStore.save()
            PopulationStore.save()
            ResourceStore.save()
            RateStore.save()
            TechStore.save()
            ActionStore.save()
            UpgradeStore.save()
            console.log("saving")
        }, 30000)
    }

    matchResourcesToJob(resource) {
        let matchedJobs = [];
        for(var i in jobs) {
            for (var x in jobs[i]) {
                if (x == resource) {
                    matchedJobs.push(i);
                }
            }
        }
        return matchedJobs
    }

    matchJobToResources(job) {
        for(var i in jobs) {
            if (i == job) {
                return jobs[i]
            }
        }
    }

    onResourceChange() {
        resources = ResourceStore.getAll()
    }

    onPopulationChange = () => {
        population = PopulationStore.getAll();
        for(var key in resources) {
           this.reloadInterval(key);
        }
    }

    reloadInterval = (resource) => {
        clearInterval(intervals[resource]);
        this.updateRate(resource);
    }

    calculateBaseRate = (listOfJobs, resource) => {
        let baseRate = 0;
        for(var i in listOfJobs) {
            const jobTitle = listOfJobs[i]
            baseRate += (parseFloat(jobs[jobTitle][resource],10) * parseInt(population[jobTitle], 10));
        }
        return baseRate;
    }

    updateRate(resource) {
        const job = this.matchResourcesToJob(resource);
        const baseRate = this.calculateBaseRate(job, resource);
        intervals[resource] = setInterval(function (resource, baseRate) {
            updateResource(resource, resources[resource], (baseRate * rates[resource] * .20));
        }.bind(null, resource, baseRate), 200);
    }
}
