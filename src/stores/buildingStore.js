import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';
import buildings from '../config/buildings.json'

const CHANGE_EVENT = 'change';

let buildingsBase = JSON.parse(JSON.stringify(buildings));
let _nodes = buildings;

class BuildingStoreClass extends EventEmitter {

    constructor() {
        super()
        this.load();

        setTimeout(() => {
            //emit change for load after all components have added listeners
            this.emitChangeAll(CHANGE_EVENT);
        },200)
    }

    emitChange(resource = CHANGE_EVENT) {
        this.emit(resource);
    }

    emitChangeAll() {
        this.emitChange()
        for (var i in _nodes) {
            this.emitChange(i);
        }
    }

    emitChangeByType(type) {
        this.emit(type)
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    addSpecificListener(building, callback) {
        this.on(building, callback);
    }

    addChangeListenerType(type, callback) {
        this.on(type, callback);
    }

    get(id) {
        return _nodes[id];
    }

    getAll() {
        return _nodes;
    }

    getNewCost(building, amount) {
        let newCost = {}

        for (var i in buildings[building].cost) {
            newCost[i] = buildingsBase[building].cost[i] * buildings[building].costMultiplier[i] * amount + parseInt(buildingsBase[building].cost[i], 10)
        }
        return newCost
    }

    isUnlocked (item) {
        let isUnlocked = false;
        for (var building in _nodes) {
            if(_nodes[building].type == 'workplace' && parseInt(_nodes[building].amount) > 0 && _nodes[building].awards.job.includes(item)) {
                isUnlocked = true;
            }
        }

        return isUnlocked;
    }

    save() {
        localStorage.setItem('building', JSON.stringify(_nodes));
    }
    load() {
        if (!!localStorage.getItem("building"))
        _nodes = JSON.parse(localStorage.getItem("building"));
    }
};

let BuildingStore = new BuildingStoreClass();

BuildingStore.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;

    switch(action.type) {

        case "UPDATE_BUILDING":
            _nodes[action.building] = action.value;
            BuildingStore.emitChange(action.building);
            break;

        case "ADD_BUILDING":
            _nodes[action.building].amount = parseInt(_nodes[action.building].amount, 10) + parseInt(action.value, 10);
            _nodes[action.building].cost = BuildingStore.getNewCost(action.building, _nodes[action.building].amount);
            BuildingStore.emitChangeByType(_nodes[action.building].type);
            BuildingStore.emitChange(action.building);
            BuildingStore.emitChange(CHANGE_EVENT);
            break;

        default:
        // do nothing
    }

});

export default BuildingStore