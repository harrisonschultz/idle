import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';
import upgrades from '../config/upgrades.json'

const CHANGE_EVENT = 'change';

let _nodes = upgrades;

class UpgradeStoreClass extends EventEmitter {

    constructor(){
        super();
        this.load()

        setTimeout(() => {
            //emit change for load after all components have added listeners
            this.emitChangeAll();
        },200)
    }
    
    emitChangeAll() {
        this.emitChange()
        for (var i in _nodes) {
            this.emitChange(i);
        }
    }

    emitChange(change = CHANGE_EVENT) {
        this.emit(change);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    get(id) {
        return _nodes[id];
    }

    getAll() {
        return _nodes;
    }

    save() {
        localStorage.setItem('upgrade', JSON.stringify(_nodes));
    }

    load() {
        if (!!localStorage.getItem("upgrade"))
            _nodes = JSON.parse(localStorage.getItem("upgrade"));
    }
};

let UpgradeStore = new UpgradeStoreClass();

UpgradeStore.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;
    switch(action.type) {
        case "ADD_UPGRADE":
            _nodes[action.name].purchased = true;
            UpgradeStore.emitChange();
            break;

        default:
        // do nothing
    }
});



export default UpgradeStore