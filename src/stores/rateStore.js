import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';
import upgradeStore from './upgradeStore';
import rates from '../config/rates.json'

const CHANGE_EVENT = 'upgrade_change';

let _nodes = rates;

class RateStoreClass extends EventEmitter {
    constructor() {
        super();
        this.load();
        upgradeStore.addChangeListener(this.calculateNewRates);

        setTimeout(() => {
            //emit change for load after all components have added listeners
            this.emitChangeAll();
        },200)
    }
    
    emitChangeAll() {
        this.emitChange()
        for (var i in _nodes) {
            this.emitChange(i);
        }
    }

    calculateNewRates() {
        const upgrades = upgradeStore.getAll();
        for(var key in upgrades) {
            if (upgrades[key].purchased == true) {
                for(var rate in upgrades[key].awards.rates){
                    _nodes[rate] = _nodes[rate] + upgrades[key].awards.rates[rate];
                }
            }
        }
        this.emitChange(CHANGE_EVENT)
    }

    emitChange(resource = CHANGE_EVENT) {
        this.emit(resource);
    }

    addChangeListener(callback, resource = CHANGE_EVENT) {
        this.on(resource, callback);
    }

    get(id) {
        return _nodes[id];
    }

    getAll() {
        return _nodes;
    }

    save() {
        localStorage.setItem('rate', JSON.stringify(_nodes));
    }

    load() {
        if (!!localStorage.getItem("rate"))
            _nodes = JSON.parse(localStorage.getItem("rate"));
    }
};

let RateStore = new RateStoreClass();

RateStore.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;

    switch(action.type) {

        case "UPDATE_RATE":
        _nodes[action.resource] = action.value;
        RateStore.emitChange(action.resource);
        break;

        default:
        // do nothing
    }

});

export default RateStore