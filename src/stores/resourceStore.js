import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';
import resources from '../config/resources.json';
import maxValues from '../config/maxValues.json';
import BuildingStore from './buildingStore';

const CHANGE_EVENT = 'change';

let _nodes = resources;

class ResourceStoreClass extends EventEmitter {

    constructor() {
        super();
        this.calculateMaxes(); 
        this.load();
        BuildingStore.addChangeListenerType('storage', this.calculateMaxes);

        setTimeout(() => {
            //emit change for load after all components have added listeners
            this.emitChangeAll();
        },200)
    }

    emitChange(resource = CHANGE_EVENT) {
        this.emit(resource);
    }
    
    emitChangeAll() {
        this.emitChange()
        for (var i in _nodes) {
            this.emitChange(i);
        }
    }

    addChangeListener(callback, resource = CHANGE_EVENT) {
        this.on(resource, callback);
    }

    get(id) {
        return _nodes[id];
    }

    getMax(id) {
        return maxValues[id];
    }

    getBuildings() {
        return BuildingStore.getAll();
    }

    isEnough(resource, amount) {
        return (_nodes[resource] + 1 >= amount)
    }

    getAll() {
        return _nodes;
    }

    calculateMaxes() {
        const buildings = BuildingStore.getAll();
        for (var i in buildings) {
            if (buildings[i].type == 'storage') {
                for (var resource in buildings[i].awards.maxes) {
                    maxValues[resource] = 20 + (buildings[i].amount * parseInt(buildings[i].awards.maxes[resource], 10));
                    this.emit(resource);
                }
            }
        }
    }

    save() {
        localStorage.setItem('resource', JSON.stringify(_nodes));
    }

    load() {
        if (!!localStorage.getItem("resource"))
            _nodes = JSON.parse(localStorage.getItem("resource"));
    }
};

let ResourceStore = new ResourceStoreClass();

ResourceStore.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;
    let sum = 0;
    switch(action.type) {
        case "ADD_RESOURCE" :
            if ((action.value + _nodes[action.resource]) <= parseInt(maxValues[action.resource])) {
                _nodes[action.resource] = (action.value + _nodes[action.resource])
                ResourceStore.emitChange(action.resource);
            } else {
            //if the new value is higher than the max, set it to the max.
                _nodes[action.resource] = parseInt(maxValues[action.resource]);
                ResourceStore.emitChange(action.resource);
            }
            break;
        case "UPDATE_RESOURCE":
            //if the new value is less than the max, set it.
            if (action.value <= parseInt(maxValues[action.resource])) {      
                _nodes[action.resource] = action.value;
                ResourceStore.emitChange(action.resource);
            } else {
            //if the new value is higher than the max, set it to the max.
                _nodes[action.resource] = parseInt(maxValues[action.resource]);
                ResourceStore.emitChange(action.resource);
            }
            break;

        case "SUBTRACT_RESOURCE":
            sum = _nodes[action.resource] - action.value;
            if (sum > -1) {
                _nodes[action.resource] = sum;
                ResourceStore.emitChange(action.resource);
            }
            break;
        default:
        // do nothing
    }

});

export default ResourceStore