import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';
import Population from '../config/population.json'
import Military from '../config/military.json'
import BuildingStore from './buildingStore';

const CHANGE_EVENT = 'population';

let _nodes = {}
_nodes.civilians = Population;
_nodes.military = {};
let maxes = {population: 0}
for(var i in Population) {
    _nodes.civilians[i] = parseInt(Population[i], 10);
}
for (var i in Military) {
    _nodes.military[i] = 0;
}

class PopulationStoreClass extends EventEmitter {
    constructor() {
        super();
        this.load();

        BuildingStore.addChangeListener(this.calculateMaxes);

        setTimeout(() => {
            //emit change for load after all components have added listeners
            this.emitChangeAll();
        },200)
    }

    getMaxes() {
        return maxes;
    }

    getMax(name) {
        return maxes[name]
    }

    emitChangeAll() {
        this.emitChange()
        for (var i in _nodes.civilians) {
            this.emitChange(i);
        }
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    emitError() {
        this.emit('no_idle');
    }

    addChangeListener(callback) {
        this.on('population', callback);
    }

    get(id) {
        return _nodes.civilians[id];
    }

    getMilitary() {
        return _nodes.military;
    }

    getMilitaryJob(job) {
        return _nodes.military[job]
    }

    getAll() {
        return _nodes.civilians;
    }

    save() {
        localStorage.setItem('population', JSON.stringify(_nodes));
    }

    load() {
        if (!!localStorage.getItem("population"))
            _nodes = JSON.parse(localStorage.getItem("population"));
    }

    calculateMaxes() {
        const buildings = BuildingStore.getAll();
        //reset all max values and recalculate them
        for (var x in maxes) {
            maxes[x] = 0;
        }
        for (var i in buildings) {
            switch(buildings[i].type) {
                case 'housing':
                    maxes.population += Math.floor(buildings[i].amount * buildings[i].awards.maxes.population)
                break;
                case 'workplace':
                    for (var item in buildings[i].awards.maxes) {
                        maxes[item] += Math.floor(buildings[i].amount * buildings[i].awards.maxes[item]);
                    }
                break;
                case 'barracks':
                        maxes.military += Math.floor(buildings[i].amount * buildings[i].awards.maxes.military);
                break;
            }
        }
        this.emit('population');
    }
};

let PopulationStore = new PopulationStoreClass();

PopulationStore.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;
    switch(action.type) {
        case "UPDATE_POPULATION":
            if(_nodes.civilians.Idle > 0) {
                _nodes.civilians.Idle--;
                _nodes.civilians[action.job]++;
                PopulationStore.emitChange();
            } else {
                PopulationStore.emitError();
            }
            break;

        case "REMOVE_POPULATION":
            if (parseInt(_nodes.civilians[action.job]) > 0){
                _nodes.civilians.Idle++;
                _nodes.civilians[action.job]--;
                PopulationStore.emitChange();
            }
            break;

        case "CREATE_POPULATION":
            _nodes.civilians.Idle++;
            PopulationStore.emitChange();
            break;

        case "UPDATE_MILITARY":
            console.log('asdf');
            console.log(_nodes);
            if(_nodes.civilians.Idle > 0) {
                console.log(action.job)
                _nodes.civilians.Idle--;
                _nodes.military[action.job] += 1;
                PopulationStore.emitChange();
            } else {
                PopulationStore.emitError();
            }
        break;

        case "REMOVE_MILITARY":
            if (parseInt(_nodes.military[action.job]) > 0){
                _nodes.civilians.Idle++;
                _nodes.military[action.job]--;
                PopulationStore.emitChange();
            }
        break;

        default:
        // do nothing
    }
});

export default PopulationStore