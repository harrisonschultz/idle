import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';
import tech from '../config/tech.json'

const CHANGE_EVENT = 'change';
const savedData = localStorage.getItem("tech");
let _nodes = tech;

//assign saved data if exists
if (savedData != null) {
    _nodes = JSON.parse(savedData)
}

class TechStoreClass extends EventEmitter {

    constructor() {
        super()
        this.load();

        setTimeout(() => {
            //emit change for load after all components have added listeners
            this.emitChange();
        },200)
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeSpecificListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    get(id) {
        return _nodes[id];
    }

    getAll() {
        return _nodes;
    }

    isUnlocked (item) {
        let isUnlocked = false;
        
        for (var tech in _nodes) {
            if(!!_nodes[tech].isUnlocked && _nodes[tech].awards.includes(item)) {
                isUnlocked = true;
            }
        }
        return isUnlocked;
    }

    save() {
        localStorage.setItem('tech', JSON.stringify(_nodes));
    } 

    load() {
        if (!!localStorage.getItem("tech"))
            _nodes = JSON.parse(localStorage.getItem("tech"));
        console.log('loaded  ', _nodes)
    }
};

let TechStore = new TechStoreClass();

TechStore.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;

    switch(action.type) {

        case "UPDATE_TECH":
        _nodes[action.resource] = action.value;
        TechStore.emitChange();
        break;

        default:
        // do nothing
    }

});

export default TechStore