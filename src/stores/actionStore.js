import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';
import actions from '../config/actions.json'

const CHANGE_EVENT = 'change';

let actionsBase = JSON.parse(JSON.stringify(actions));
let _nodes = actions;

class ActionStoreClass extends EventEmitter {
    
    constructor() {
        super()
        this.load();

        setTimeout(() => {
            //emit change for load after all components have added listeners
            this.emitChangeAll(CHANGE_EVENT);
        },200)
    }

    emitChange(changeEvent = CHANGE_EVENT) {
        this.emit(changeEvent);
    }

    emitChangeAll() {
        this.emitChange()
        for (var i in _nodes) {
            this.emitChange(i);
        }
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    addSpecificChangeListener(callback,action) {
        this.on(action, callback);
    }

    get(id) {
        return _nodes[id];
    }

    getAll() {
        return _nodes;
    }

    getNewCost(action) {
        let newCost = {}

        for (var i in actions[action].cost) {
            if (!!actions[action].costMultiplier && actions[action].costMultiplier[i]){
                newCost[i] = actionsBase[action].cost[i] * actions[action].costMultiplier[i] * _nodes[action].amount + parseInt(actionsBase[action].cost[i], 10)
            } else {
                newCost[i] = actionsBase[action].cost[i]
            }
        }
        return newCost
    }

    save() {
        localStorage.setItem('action', JSON.stringify(_nodes));
    }

    load() {
        if(!!localStorage.getItem('action')) {
            _nodes = JSON.parse(localStorage.getItem('action'));
        }
    }
};

let ActionStore = new ActionStoreClass();

ActionStore.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;
    switch(action.type) {
        
        case "ADD_ACTION":
            _nodes[action.action].amount = parseInt(_nodes[action.action].amount, 10) + parseInt(action.value, 10);
            _nodes[action.action].cost = ActionStore.getNewCost(action.action);
            ActionStore.emitChange(action.action);
            ActionStore.emitChange();
            break;

        default:
        // do nothing
    }
});



export default ActionStore