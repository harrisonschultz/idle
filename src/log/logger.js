import AppDispatcher from '../dispatcher/dispatcher';
import EventEmitter from 'events';

const CHANGE_EVENT = "log_change"
let logs = {}

class LoggerClass extends EventEmitter {
    constructor() {
        super()
        setInterval(() => {
            this.deleteOldLogs();
        },300000);
    }

    addEventListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    getAll() {
        return logs;
    }

    getNewLogs() {

    }

    getRecentLogs() {

    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    deleteOldLogs() {
        //delete logs that are at least five minutes old
        const fiveMinAgo = new Date().getTime() - 300000;
        let newLogs = [];
        for (var i in logs) {
            if(i > fiveMinAgo) {
                newLogs[i] = logs[i];
            }
        }
        logs = newLogs;
    }
}

let Logger = new LoggerClass();

Logger.dispatchToken = AppDispatcher.register(function(payload) {
    const action = payload.action;
    switch(action.type) {
        
        case "LOG":
            logs[new Date().getTime()] = action.message;
            Logger.emitChange(CHANGE_EVENT);
            break;

        default:
        // do nothing
    }
});

export default Logger