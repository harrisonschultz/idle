import React from 'react';
import Logger from './logger'

export default class Log extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            logs: {}
        }
    }

    componentDidMount() {
        Logger.addEventListener(this.updateLogs);
    }

    updateLogs = () => {
        this.setState({logs: Logger.getAll()});
    }

    createLogs = () => {
        return Object.keys(this.state.logs).sort().map(timestamp => {
            const date = new Date(parseInt(timestamp)).toLocaleTimeString()
            // console.log(timestamp);
            return (
                <div key={date}>
                    {date + " - " + this.state.logs[timestamp]}
                </div>
            )
        })
    }

    render() {
        return (
            <div className="LogWindow">
                {this.createLogs()}
            </div>
        )
    }
}