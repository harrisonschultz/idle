import AppDispatcher from '../dispatcher/dispatcher.js';

export function log(message) {
    AppDispatcher.handleServerAction({
        type: "LOG",
        message
    });
}