import AppDispatcher from '../dispatcher/dispatcher.js';

export function addAction(action, amount = 1) {
    AppDispatcher.handleServerAction({
        type: "ADD_ACTION",
        action,
        value: amount
    });
}