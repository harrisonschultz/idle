import AppDispatcher from '../dispatcher/dispatcher.js';

export function updateBuilding(building, valueToAdd) {
    AppDispatcher.handleServerAction({
        type: "UPDATE_BUILDING",
        building,
        value: valueToAdd
    });
}

export function addBuilding(building, amount = 1) {
    AppDispatcher.handleServerAction({
        type: "ADD_BUILDING",
        building,
        value: amount
    });
}