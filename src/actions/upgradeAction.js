import AppDispatcher from '../dispatcher/dispatcher.js';

export function purchaseUpgrade(name) {
    AppDispatcher.handleServerAction({
        type: "ADD_UPGRADE",
        name
    });
}