
import AppDispatcher from '../dispatcher/dispatcher.js';

export function updateTech(resource, value) {
    AppDispatcher.handleServerAction({
        type: "UPDATE_TECH",
        resource,
        value
    });
}