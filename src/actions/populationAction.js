import AppDispatcher from '../dispatcher/dispatcher.js';

export function addNewPopulation(job) {
    AppDispatcher.handleServerAction({
        type: 'CREATE_POPULATION',
        job
    });
}

export function updatePopulation(job) {
    AppDispatcher.handleServerAction({
        type: 'UPDATE_POPULATION', 
        job
    });
}

export function removePopulation(job) {
    AppDispatcher.handleServerAction({
        type: 'REMOVE_POPULATION', 
        job
    });
}

export function updateMilitary(job) {
    AppDispatcher.handleServerAction({
        type: 'UPDATE_MILITARY', 
        job
    });
}

export function removeMilitary(job) {
    AppDispatcher.handleServerAction({
        type: 'REMOVE_MILITARY', 
        job
    });
}