import AppDispatcher from '../dispatcher/dispatcher.js';
import ResourceStore from '../stores/resourceStore';

export function updateResource(resource, valueToAdd, currentValue = 0) {
    AppDispatcher.handleServerAction({
        type: "UPDATE_RESOURCE",
        resource,
        value: parseFloat(currentValue, 10) + parseFloat(valueToAdd)
    });
}

export function addResource(resource, valueToAdd) {
    AppDispatcher.handleServerAction({
        type: "ADD_RESOURCE",
        resource,
        value: parseFloat(valueToAdd)
    });
}

export function subtractResource(resource, value) {
    if (ResourceStore.isEnough(resource, value)) {
        AppDispatcher.handleServerAction({
            type: "SUBTRACT_RESOURCE",
            resource,
            value: parseInt(value, 10)
        });
        return true;
    } else {
        return false;
    }
}

export function checkResources(resources) {
    for (var i in resources) {
        if (!ResourceStore.isEnough(i, resources[i])) {
            return false;
        }
    }
    return true;
}