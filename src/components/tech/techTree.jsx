import React from 'react';
import TechStore from '../../stores/techStore.js'
import TechItem from './techItem'
import techTree from '../../config/tech.json'

let refs = {};
let renderedNodes;
let sortedTree = [];
let firstRender = true;

//This component uses logic to cause renders to happen twice. First to render all nodes in the tree, then once they are rendered it uses the references to those rendered nodes to position svg lines to each node
export default class TechTree extends React.Component {

    getStateFromStores = () => {
        const techToDisplay = this.categorizeTechs(TechStore.getAll());
        return {
            techTree: techToDisplay
        };
    }

    onChange = () => {
        firstRender = true;
        this.setState(this.getStateFromStores);
    }

    constructor(props) {
        super(props)

        const techToDisplay = this.categorizeTechs(techTree);

        this.state = {
            nodes: [],
            techTree: techToDisplay
        }
    }

    onResize = () => {
        firstRender = true;
        this.forceUpdate();
    }

    componentDidMount() {
        TechStore.addChangeListener(this.onChange);
        window.addEventListener("resize", this.onResize);
        this.resetRenderCycle();
        
        const techToDisplay = this.categorizeTechs(TechStore.getAll());

        this.setState({nodes: renderedNodes, techTree:techToDisplay})
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
        TechStore.removeSpecificListener(this.onChange);
    }

    createLines(index) {
        if (Object.keys(refs).length > 0) {
            let lines = [];

            for (var key in sortedTree[index + 1]) {
                const parentTech = this.state.techTree[key].parentTech[0]
                if (!!refs[key]){
                lines.push(
                    <line 
                        x1={refs[parentTech].offsetLeft + refs[parentTech].clientWidth/2} 
                        y1={0} 
                        x2={refs[key].offsetLeft + refs[key].clientWidth/2} 
                        y2='100%'
                        strokeWidth="2"
                        stroke="white"
                        key={key}
                    />
                )
                } else {
                    //if refs[key] does not exist then that ref has yet to be created. This causes the render cycle to repeat which will guarantee the ref is created when trying to access it
                    this.resetRenderCycle();
                }
            }
                
            return (
                <div className='LineDiv'>
                    <svg width="100%" height='20px'>
                        {lines}
                    </svg>
                </div>
            )
        }
    }

    componentDidUpdate = () => {
        if(firstRender) {
            firstRender = false;
            this.addBranchLines();
        }
    }

    addRefs = (ref) => {
        if (!!ref) {
            Object.defineProperty(refs, ref.id, {value: ref, writeable: true, enumerable: true, configurable: true})
        }
    }

    addBranchLines = () => {
        let newTree = renderedNodes.slice();
        for (var i = 0; i < (sortedTree.length-2); i++) {
            let newIndex = i * 2 + 1;
            newTree.splice(newIndex, 0, (
                <div className='row center'  key={"lines" + i}>
                    {this.createLines(i)}
                </div>
            ))
        }
        this.setState({nodes: newTree})
    }

    createTree = () => {
        sortedTree = this.groupByRow();
        sortedTree.splice(0,1); //remove the initial tech from the display

        let tree = Object.keys(sortedTree).map((key) => {
            return (
                <div className="TreeRow row" key={key} >
                    {this.createRow(sortedTree[key])}
                </div>
            )
        })
        renderedNodes = tree;
    }

    createRow = (technologies) => {
        let row = Object.keys(technologies).map((key) => {
            return (
                <div key={key} id={key} ref={this.addRefs}>
                    <TechItem
                        tech={technologies[key]}
                        name={key}
                        />
                </div>
            )
        })
        return row
    }

    groupByParent() {
        let techSorted = {};

        Object.keys(this.state.techTree).map(key => {
            for (var i in this.state.techTree[key].parentTech) {
                if (!techSorted[this.state.techTree[key].parentTech[i]]) {
                    techSorted[this.state.techTree[key].parentTech[i]] = {};
                }
                techSorted[this.state.techTree[key].parentTech[i]][key] = this.state.techTree[key];
            }
        })
        return techSorted
    }

    groupByRow(techSorted = [{"Gathering": this.state.techTree[Object.keys(this.state.techTree)[0]]}], techsLeft = this.removeElement(Object.keys(this.state.techTree), 'Gathering'), index = 0) {
        let techsToBeDeleted = [];
        techSorted.push([]); // initialize the next row;
        
        for (var key in techsLeft) { //look at all techs that have yet to be put in the tree structure
            const parents = this.state.techTree[techsLeft[key]].parentTech
            for (var i in parents) {
                if (Object.keys(techSorted[index]).includes(parents[i])){ //If the parent exists in the previous set of nodes (row) then put it in this set.
                    Object.defineProperty(techSorted[index+1], techsLeft[key], { value: this.state.techTree[techsLeft[key]], writeable: true, enumerable: true, configurable: true})
                    techsToBeDeleted.push(techsLeft[key])
                }
            }
        }

        if(techsLeft.length < 1) {
            return techSorted
        }

        for(var deletedTech in techsToBeDeleted) {
            techsLeft = this.removeElement(techsLeft, techsToBeDeleted[deletedTech]);
        }
        index = index + 1;
        return this.groupByRow(techSorted, techsLeft, index);
    }

    removeElement(array, element) {
        return array.filter(e => e !== element);
    }

    categorizeTechs(techList) {
        let techToDisplay = {};
        for (var i in techList) {
            if(techList[i].category == this.props.category){
                techToDisplay[i] = techList[i]
            }
        }
        return techToDisplay;
    }

    resetRenderCycle() {
        firstRender = true;
        refs = {};
    }

    render() {
        if (firstRender)
            this.createTree();

        return (
            <div>
                {this.state.nodes}
            </div>
        );
    }
}