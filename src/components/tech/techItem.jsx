import React from 'react';

import TechStore from '../../stores/techStore.js'
import {updateTech} from '../../actions/techAction'
import { subtractResource, checkResources } from '../../actions/resourceAction.js'
export default class TechItem extends React.Component {
    refs = {}

    constructor(props) {
        super(props) 

        this.state = {
            details: '',
            unlocked: false
        }
    }

    addTech = () => {
        this.props.tech.isUnlocked = true //setting to true because they is no reason to copy the object
        updateTech(this.props.name, this.props.tech)
    }

    getStateFromStores = () => {
        return {
            tech: TechStore.get(this.props.tech),
        };
    }

    componentWillUpdate = () => {
        // console.log('eeeee')
    }


    onChange = () => {
        this.setState(this.getStateFromStores());
    }

    createCosts = (cost) => {
        return Object.keys(cost).map((resource) => {
            return (
                <div key={resource} className="Cost">
                    {resource + ": " + cost[resource]}
                </div>
            )
        })
    }

    createAwards = (awards) => {
        return awards.map((technology) => {
            return (
                <div key={technology} className="Award">
                    {technology.replace("_", " ")}
                </div>
            )
        })
    }

    purchase = () => {
        if (checkResources(this.props.tech.cost)) {
            for (var key in this.props.tech.cost) {
                if (!subtractResource(key, this.props.tech.cost[key])) {
                    this.errorResource = key
                    this.setState({displayError: true})
                    return
                }
            }
            this.addTech(this.props.name);
            this.setState({unlocked: true});
        }
    }

    showDetails = (event) => {
        this.setState({
            details:(
            <div className="TechDetails" style={{left: this.refs.offsetLeft + 15, top: this.refs.offsetTop + 26, position: "absolute"}}>
                <div className='TechTitle'>Cost</div>
                <div className='TechCost TechPrint'>
                    {this.createCosts(this.props.tech.cost)}
                </div>
                <hr/>
                <div className='TechTitle'>Awards</div>
                <div className='TechAwards TechPrint' >
                    {this.createAwards(this.props.tech.awards)}
                </div>
            </div>
        )})
    }

    hideDetails = () => {
        this.setState({details: ""})
    }

    createTechBox = () => {
        if(!!this.props.tech) {
            let name = this.props.name.replace("_", " ");
            if (this.state.unlocked || !!this.props.tech.isUnlocked) {
                name = (<strike> {this.props.name.replace("_", " ")} </strike>)
            }
            return (
                <div className='TechCell TechBar Clickable' onMouseEnter={this.showDetails} onMouseLeave={this.hideDetails}>
                    <div className='TechName row'>
                        {name}
                    </div>
                </div>
            )
        }
    }

    render() {
        // console.log('asdf' + !!this.props.tech.isUnlocked)
        return (
            <div ref={ref => {this.refs = ref}} onClick={this.purchase}>
                {this.createTechBox()}
                {this.state.details}
            </div>
        );
    }
}