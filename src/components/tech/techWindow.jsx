import React from 'react';
import TechTree from './techTree'
import technologies from '../../config/tech.json'

export default class TechWindow extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            categories: {},
        }
        this.state.currentTab = this.getAllCategories();
    }

    getAllCategories() {
        let newCategories = this.state.categories
        for (var i in technologies) {
            if (!!technologies[i].category && Object.keys(newCategories).indexOf(technologies[i].category) < 0) {
                newCategories[technologies[i].category] = false;
            }
        }
        this.state.categories = newCategories
        newCategories[Object.keys(newCategories)[0]] = true;
        return Object.keys(newCategories)[0]
    }

    createAllTrees() {
        return (
            <div key={this.state.currentTab + "Tree"} className='col-md-12'> 
                <div className="row">
                    <div className="col-md-12">
                        <TechTree
                            category={this.state.currentTab}
                        />
                    </div>
                </div>
            </div>
        )
    }

    createTabs() {
        return Object.keys(this.state.categories).map(element => {
            if (this.state.categories[element]){
                return (
                    <div className="TechTab Clickable activeTab" key={element + "Tab"} onClick={() => this.switchTab(element)}>
                        {element}
                    </div>
                )
            } else {
                return (
                    <div className="TechTab Clickable" key={element + "Tab"} onClick={() => this.switchTab(element)}>
                        {element}
                    </div>
                )
            }
        })
    }

    switchTab = (element) => {
        let newCategories = {}
        if (element == this.state.currentTab) 
            return
        for (var i in this.state.categories) {
            newCategories[i] = false;
        }
        newCategories[element] = true;
        this.setState({categories: newCategories, currentTab: element});
    }

    render() {
        return (
            <div>
                <div className="row TechTabRow CenterChildren">
                    {this.createTabs()}
                </div>
                <div className="row CenterChildren">
                    {this.createAllTrees()}
                </div>
            </div>
        );
    }
}