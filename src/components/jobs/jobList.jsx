import React from 'react';

import PopulationStore from '../../stores/populationStore.js'
import BuildingStore from '../../stores/buildingStore.js' //Buildings unlock new Jobs
import TechStore from '../../stores/techStore.js' //Buildings unlock new Jobs
import Job from './job'
import military from '../../config/military.json'
import jobList from '../../config/jobs.json'

let jobSet = jobList

export default class JobList extends React.Component {

    componentWillMount() {
        this.onPopulationChange();
    }

    componentDidMount() {
        BuildingStore.addChangeListener(this.onBuildingChange);
        PopulationStore.addChangeListener(this.onPopulationChange);
        TechStore.addChangeListener(this.onTechChange);
    }

    getPopulationStateFromStores = () => {
        return {
            population: PopulationStore.getAll(),
        };
    }

    getBuildingStateFromStores = () => {
        return {
            buildings: BuildingStore.getAll(),
        };
    }

    onBuildingChange = () => {
        this.setState(this.getBuildingStateFromStores());
    }
    onPopulationChange = () => {
        this.setState(this.getPopulationStateFromStores());
    }
    onTechChange = () => {
        //re-render on tech change to diplay new unlocks
        this.forceUpdate()
    }

    createTable = () => {
        let table = Object.keys(jobSet).sort().map((key) => {
            if (TechStore.isUnlocked(key) || BuildingStore.isUnlocked(key)) {
                return (
                    <div key={key} className='JobDiv'>
                        <Job 
                            job={key}
                            military={this.props.military}
                        />
                    </div>
                )
            }
        })
      
        return table
    }

    render() {
        if (this.props.military) {
            jobSet = military
        } else {
            jobSet = jobList
        }

        return (
            <div>
                {this.createTable()}
            </div>
        );
    }
}