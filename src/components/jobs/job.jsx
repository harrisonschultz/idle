import React from 'react';
import {updatePopulation, removePopulation, updateMilitary, removeMilitary} from '../../actions/populationAction';

export default class Job extends React.Component {

    hire = () => {
        if (this.props.military) {
            updateMilitary(this.props.job);
        } else {
            updatePopulation(this.props.job)
        }
    }

    fire = () => {
        if (this.props.military) {
            removeMilitary(this.props.job);
        } else {
            removePopulation(this.props.job)
        }
    }

    render() {
        return (
            <div className='ResourceCell ResourceBar'>
                <div className='ResourceName'>
                    {this.props.job}
                </div>
                <div className='JobAction Clickable' onClick={this.hire}>
                   <i className="fa fa-plus"></i>
                </div>
                <div className='JobAction Clickable' onClick={this.fire}>
                    <i className="fa fa-minus"></i>
                </div>
            </div>
        );
    }
}