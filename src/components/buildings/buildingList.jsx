import React from 'react';
import TechStore from '../../stores/techStore.js'
import Building from './building'
import Buildings from '../../config/buildings.json'

export default class buildingList extends React.Component {

    componentDidMount() {
        TechStore.addChangeListener(this.onChange);
    }

    componentWillUnmount() {
        TechStore.removeSpecificListener(this.onChange);
    }

    onChange = () => {
        //re render on tech update if something was unlocked
        this.forceUpdate();
    }

    createTable = () => {
        let table = Object.keys(Buildings).sort().map((key) => {
            if (TechStore.isUnlocked(key)) {
                return (
                    <div key={key}>
                        <Building
                            building={Buildings[key]}
                            name={key}
                        />
                    </div>
                    )
            }
        })
      
        return table
    }

    render() {
        return (
            <div>
                {this.createTable()}
            </div>
        );
    }
}