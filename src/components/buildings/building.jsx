import React from 'react';
import { addBuilding } from '../../actions/buildingAction'
import { subtractResource, checkResources } from '../../actions/resourceAction'
import BuildingStore from '../../stores/buildingStore'

export default class Building extends React.Component {
    refs = {}
    errorResource = '';

    constructor(props) {
        super(props)
        
        this.state = {
            details: '',
            building: {},
            displayError: false,
            showTooltip: false
        }
    }

    addRef = ref => {
        this.refs = ref;
    }

    componentDidMount() {
        this.onChange();
        BuildingStore.addSpecificListener(this.props.name, this.onChange);
    }

    componentWillUnmount() {
        BuildingStore.removeListener(this.props.name, this.onChange)
    }

    onChange = () => {
       this.setState({building: BuildingStore.get(this.props.name)});
    }

    purchase = () => {
        if (checkResources(this.state.building.cost)) {
            for (var key in this.state.building.cost) {
                if (!subtractResource(key, this.state.building.cost[key])) {
                    this.errorResource = key
                    this.setState({displayError: true})
                    return
                }
            }
            addBuilding(this.props.name);
        }
    }

    createDetails = (objectToDisplay) => {
        return Object.keys(objectToDisplay).map( resource => {
            return (
                <div key={resource} className="Cost">
                    {resource.replace("_", " ") + ": " + objectToDisplay[resource]}
                </div>
            )
        })
    }
    createAwards = (objectToDisplay) => {
        return Object.keys(objectToDisplay).map(resource => {
            return Object.keys(objectToDisplay[resource]).map(awards => {
                if(resource == 'maxes') {
                    return (
                        <div key={awards} className="Cost">
                            {"Max " + awards.replace("_", " ") + ": +" + objectToDisplay[resource][awards]}
                        </div>
                    )
                } else if(resource == 'job') {
                    return (
                        <div key={awards} className="Cost">
                            {objectToDisplay[resource][awards]}
                        </div>
                    )
                } 
                return (
                    <div key={awards} className="Cost">
                        {awards.replace("_", " ") + ": " + objectToDisplay[resource][awards]}
                    </div>
                )
            })
        })
    }

    showError() {  
        //Error displays for 3 seconds
        setTimeout(() => {
            this.setState({displayError: false})
        }, 3000)
        
        return (
            <div className="buildingError">
                {"Not Enough " + this.errorResource}
            </div>
        )
    }

    createTooltip = (event) => {
        if (this.state.showTooltip) {
            return(
                <div className="BuildingDetails" style={{left: this.refs.offsetLeft + 15, top: this.refs.offsetTop + 26, position: "absolute"}}>
                    <div className='TechTitle'>Cost</div>
                    <div className='TechCost TechPrint'>
                        {this.createDetails(this.state.building.cost)}
                    </div>
                    <hr/>
                    <div className='TechTitle'>Awards</div>
                    <div className='TechAwards TechPrint' >
                        {this.createAwards(this.props.building.awards)}
                    </div>
                </div>
            ) 
        } else {
            return ""
        }
    }

    hideDetails = () => {
        this.setState({showTooltip: false})
    }

    showDetails = () => {
        this.setState({showTooltip: true})
    }
    
    render() {
        return (
            <div className='BuildingCell'>
            {this.state.displayError && this.showError()}
                <div 
                    className="Building Clickable" 
                    onClick={this.purchase} 
                    onMouseEnter={this.showDetails} 
                    onMouseLeave={this.hideDetails}
                    ref={this.addRef}
                    >
                    {this.props.name}
                    <div className="BuildingAmount">
                        {this.state.building.amount}
                    </div>
                    {this.createTooltip()}
                </div>
                <div>
                    {this.state.details}
                </div>
            </div>
        );
    }
}