import React from 'react';
import PopulationStore from '../stores/populationStore.js'
import TechStore from '../stores/techStore.js'
import BuildingStore from '../stores/buildingStore.js';

export default class Population extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            population: {},
            military: {},
            details: '',
            showDetails: true
        }
    }

    componentWillMount() {
        this.setState(this.getStateFromStores());
    }

    componentDidMount() {
        PopulationStore.addChangeListener(this.onChange);
        BuildingStore.addChangeListener(this.update);
        TechStore.addChangeListener(this.update);
    }

    update = () => {
        this.forceUpdate();
    }

    getStateFromStores = () => {
        return {
            population: PopulationStore.getAll(),
            military: PopulationStore.getMilitary()
        };
    }

    onChange = () => {
        this.setState(this.getStateFromStores());
    }

    showDetails = () => {
        if(this.state.showDetails) {
            //if the first military unit is unlocked then display military as well
            if (TechStore.isUnlocked('Warrior')) {
                const seperator = (
                    <div className='Seperator'>
                        <b>
                            Military
                        </b>
                    </div>
                )
                return this.createCivilians().concat(seperator).concat(this.createMilitary())
            }
            return this.createCivilians();
         } else {
             return '';
         }
    }

    createCivilians = () => {
        const detailsTable = Object.keys(this.state.population).sort().map((key) => {
            if(TechStore.isUnlocked(key) || BuildingStore.isUnlocked(key)){
                return (
                    <div className='ResourceDetails' key={key}>
                        <div className='ResourceName' key={key}>
                            {key}
                        </div>
                        <div className='ResourceValue' key={key + ' '}>
                            {this.state.population[key]}
                        </div>
                    </div>
                )
            }
         })
        return detailsTable;
    }

    createMilitary = () => {
        const detailsTable = Object.keys(this.state.military).sort().map((key) => {
            if(TechStore.isUnlocked(key) || BuildingStore.isUnlocked(key)){
                return (
                    <div className='ResourceDetails' key={key}>
                        <div className='ResourceName' key={key}>
                            {key}
                        </div>
                        <div className='ResourceValue' key={key + ' '}>
                            {this.state.military[key]}
                        </div>
                    </div>
                )
            }
         })
         return detailsTable;
    }

    expand = () => {
        this.setState({showDetails: !this.state.showDetails})
    }

    getTotal = () => {
        let total = 0;
        for(var i in this.state.population) {
            total += this.state.population[i]
        }
        return total
    }

    render() {
        const total = this.getTotal();
        return (
            <div>
                <div className='ResourceBar Clickable' onClick={this.expand}>
                    <div className='ResourceName'>Population </div>
                    <div className='ResourceValue'>{total}</div>
                </div>
                <div className='ResourceDetailsBlock'>{this.showDetails()}</div>
            </div>
        );
    }
}