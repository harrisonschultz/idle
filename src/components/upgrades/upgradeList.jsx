import React from 'react';
import UpgradeStore from '../../stores/upgradeStore.js'
import BuildingStore from '../../stores/buildingStore.js' //Buildings unlock new Jobs
import TechStore from '../../stores/techStore.js' //Buildings unlock new Jobs
import Upgrade from './upgrade'
import upgradeList from '../../config/upgrades.json'

export default class UpgradeList extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            upgradeList: upgradeList
        }
    }

    onUpgradeChange() {
        this.setState({upgradeList: UpgradeStore.getAll()});
    }

    componentDidMount() {
        BuildingStore.addChangeListener(this.onBuildingChangeUpgrade);
        UpgradeStore.addChangeListener(this.onUpgradeChangeUpgrade);
        TechStore.addChangeListener(this.onTechChangeUpgrade);
    }

    componentWillUnmount() {
        BuildingStore.removeListener('change', this.onBuildingChangeUpgrade);
        UpgradeStore.removeListener('change', this.onUpgradeChangeUpgrade);
        TechStore.removeListener('change', this.onTechChangeUpgrade);
    }

    getUpgradeStateFromStores = () => {
        return {
            upgrades: UpgradeStore.getAll(),
        };
    }

    getBuildingStateFromStores = () => {
        return {
            buildings: BuildingStore.getAll(),
        };
    }

    onBuildingChangeUpgrade = () => {
        this.setState(this.getBuildingStateFromStores());
    }
    onUpgradeChangeUpgrade = () => {
        this.setState(this.getUpgradeStateFromStores());
    }
    onTechChangeUpgrade = () => {
        //re-render on tech change to diplay new unlocks
        this.forceUpdate()
    }

    createTable = () => {
        let table = Object.keys(upgradeList).sort().map((key) => {
            if (!upgradeList[key].purchased && TechStore.isUnlocked(key) || BuildingStore.isUnlocked(key)) {
                return (
                    <div key={key} className='JobDiv'>
                        <Upgrade
                            upgrade={upgradeList[key]}
                            name={key}
                        />
                    </div>
                )
            }
        })
      
        return table
    }

    render() {
        return (
            <div>
                {this.createTable()}
            </div>
        );
    }
}