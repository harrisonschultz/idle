import React from 'react';
import { purchaseUpgrade } from '../../actions/upgradeAction'
import { subtractResource, checkResources } from '../../actions/resourceAction'

export default class Upgrade extends React.Component {
    refs = {}
    errorResource = '';

    constructor(props) {
        super(props)   
        this.state = {
            details: '',
            displayError: false,
            showTooltip: false
        }
    }

    addRef = ref => {
        this.refs = ref;
    }

    purchase = () => {
        if (checkResources(this.props.upgrade.cost)) {
            for (var key in this.props.upgrade.cost) {
                if (!subtractResource(key, this.props.upgrade.cost[key])) {
                    this.errorResource = key
                    this.setState({displayError: true})
                    return
                }
            }
            purchaseUpgrade(this.props.name);
        }
    }

    createDetails = (objectToDisplay) => {
        return Object.keys(objectToDisplay).map( resource => {
            return (
                <div key={resource} className="Cost">
                    {resource.replace("_", " ") + " " + objectToDisplay[resource]}
                </div>
            )
        })
    }

    createAwards = (objectToDisplay) => {
        return Object.keys(objectToDisplay).map(resource => {
            return Object.keys(objectToDisplay[resource]).map(awards => {
                if(resource == 'maxes') {
                    return (
                        <div key={awards} className="Cost">
                            {"Max " + awards.replace("_", " ") + " +" + objectToDisplay[resource][awards]}
                        </div>
                    )
                } else if(resource == 'rates') {
                    return (
                        <div key={awards} className="Cost">
                            {awards.replace("_", " ") + " +" + (objectToDisplay[resource][awards]*100) + "%"}
                        </div>
                    )
                } 
                return (
                    <div key={awards} className="Cost">
                        {awards.replace("_", " ") + " " + objectToDisplay[resource][awards]}
                    </div>
                )
            })
        })
    }

    showError() {  
        //Error displays for 3 seconds
        setTimeout(() => {
            this.setState({displayError: false})
        }, 3000)
        
        return (
            <div className="buildingError">
                {"Not Enough " + this.errorResource}
            </div>
        )
    }

    createTooltip = (event) => {
        if (this.state.showTooltip) {
            return(
                <div className="BuildingDetails" style={{left: this.refs.offsetLeft + 15, top: this.refs.offsetTop + 30, position: "absolute"}}>
                    <div className='TechTitle'>Cost</div>
                    <div className='TechCost TechPrint'>
                        {this.createDetails(this.props.upgrade.cost)}
                    </div>
                    <hr/>
                    <div className='TechTitle'>Awards</div>
                    <div className='TechAwards TechPrint' >
                        {this.createAwards(this.props.upgrade.awards)}
                    </div>
                </div>
            ) 
        } else {
            return ""
        }
    }

    hideDetails = () => {
        this.setState({showTooltip: false})
    }

    showDetails = () => {
        this.setState({showTooltip: true})
    }
    
    render() {
        return (
            <div className='BuildingCell'>
                {this.state.displayError && this.showError()}
                <div 
                    className="Building Clickable" 
                    onClick={this.purchase} 
                    onMouseEnter={this.showDetails} 
                    onMouseLeave={this.hideDetails}
                    ref={this.addRef}
                    >
                    {this.props.name}
                    {this.createTooltip()}
                </div>
                <div>
                    {this.state.details}
                </div>
            </div>
        );
    }
}