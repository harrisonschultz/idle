import React from 'react';

export default class TabWindow extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentTab: ''
        }
    }

    componentDidMount() {
        //set the default tab on mount
        this.setState({currentTab: this.props.tabNames[0]})
    }

    createTabs() {
        return Object.keys(this.props.tabNames).map(element => {
            if (this.state.currentTab == this.props.tabNames[element]){
                return (
                    <div className="Tab Clickable activeTab" key={this.props.tabNames[element] + "Tab"} onClick={() => this.switchTab(this.props.tabNames[element])}>
                        {this.props.tabNames[element]}
                    </div>
                )
            } else {
                return (
                    <div className="Tab Clickable" key={this.props.tabNames[element] + "Tab"} onClick={() => this.switchTab(this.props.tabNames[element])}>
                        {this.props.tabNames[element]}
                    </div>
                )
            }
        })
    }

    switchTab = (element) => {
        if (element == this.state.currentTab) 
            return
        this.setState({currentTab: element});
    }

    createTabItems = () => {
        return this.props.tabItems[this.props.tabNames.indexOf(this.state.currentTab)]
    }

    render() {
        return (
            <div>
                <div className="row TechTabRow CenterChildren">
                    {this.createTabs()}
                </div>
                <div className='LightNavy'>
                    {this.createTabItems()}
                </div>
            </div>
        );
    }
}