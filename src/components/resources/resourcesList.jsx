import React from 'react';
import categories from '../../config/categories'
import TechStore from '../../stores/techStore.js'
import Resource from './resource'
import BuildingStore from '../../stores/buildingStore';

export default class ResourceList extends React.Component {
    
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        TechStore.addChangeListener(this.onChange);
        BuildingStore.addChangeListener(this.onChange);
    }

    onChange = () => {
        this.forceUpdate();
    }

    createTable = () => {
        let table = Object.keys(categories).sort().map((key) => {
            if (TechStore.isUnlocked(key) || BuildingStore.isUnlocked(key)) {
                let unlockedResources = []
                categories[key].forEach(item => {
                    if (TechStore.isUnlocked(item) || BuildingStore.isUnlocked(item)) {
                        unlockedResources.push(item)
                    }
                })
                return (
                    <div key={key}>
                        <Resource 
                            category={key}
                            resources={unlockedResources}
                        />
                    </div>
                )
            }
        })
      
        return table
    }

    render() {
        return (
            <div>
                {this.createTable()}
            </div>
        );
    }
}