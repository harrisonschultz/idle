import React from 'react';
import ResourceStore from '../../stores/resourceStore.js'
import maxValues from '../../config/maxValues.json';

export default class Resource extends React.Component {

    constructor(props) {
        super(props) 

        this.state = {
            details: true
        }
    }

    componentWillMount() {
        this.setState(this.getStateFromStores());
    }

    componentDidMount() {
        for(var i in this.props.resources) {
            ResourceStore.addChangeListener(this.getStateFromStores, this.props.resources[i]);
        }
    }

    getStateFromStores = () => {
        let resources = {};
        let maxValues = {}
        for(var i in this.props.resources) {
            resources[this.props.resources[i]] = ResourceStore.get(this.props.resources[i]);
            maxValues[this.props.resources[i]] = ResourceStore.getMax(this.props.resources[i]);
        }
        this.setState({resources, maxValues});
    }

    expand = () => {
        this.setState({details: !this.state.details});
    }

    showDetails = () => {        
        if(this.state.details) {
            return Object.keys(this.state.resources).map((key) => {
                return (
                    <div className='ResourceDetails' key={key}>
                        <div className='ResourceName' key={key}>
                            {key}
                        </div>
                        <div className='ResourceValue' key={key + " "}>
                            {Math.floor(parseInt(this.state.resources[key], 10)) + " / " + maxValues[key]}
                        </div>
                    </div>
                )
            })
        } else {
            return '';
        }
    }

    createResourceBar = () => {
        const resources = this.state.resources;
        const maxes = this.state.maxValues;

        if(!!resources && Object.keys(resources).length == 1){
            return (
                <div className='ResourceCell ResourceBar'>
                    <div className='ResourceName'>
                        {this.props.resources[0]}
                    </div>
                    <div className='ResourceValue'>
                        {Math.floor(parseInt(this.state.resources[this.props.resources[0]], 10)) + " / " + maxes[this.props.resources[0]]}
                    </div>
                </div>
            )
        } else if (!!resources) {
            //If this contains sub resources 
            let maxTotal = 0;
            let total = 0;
            Object.keys(this.props.resources).map((id) => {
                maxTotal += parseInt(maxes[this.props.resources[id]],10)
                total += resources[this.props.resources[id]];
            })
            total = Math.floor(parseInt(total,10)); //adds the tota
            return (
                <div >
                    <div className='ResourceCell ResourceBar Clickable' onClick={this.expand}>
                        <div className='ResourceName'>
                            {this.props.category}
                        </div>
                        <div className='ResourceValue'>
                            {total + " / " + maxTotal}
                        </div>
                    </div>
                    <div className='ResourceDetailsBlock'>{this.showDetails()}</div>
                </div>
            )
        }
    }
    
    render() {
        return (
            <div>
                {this.createResourceBar()}
            </div>
        );
    }
}
