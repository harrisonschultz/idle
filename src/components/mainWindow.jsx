import React from 'react';

import Population from './population';
import ResourcesList from './resources/resourcesList';
import JobList from './jobs/jobList';
import BuildingList from './buildings/buildingList';
import TechWindow from './tech/techWindow';
import ActionList from './actions/actionList';
import UpgradeList from './upgrades/upgradeList';
import TabWindow from './tabWindow';
import Log from '../log/log'

let tabItems = [(<BuildingList/>), (<UpgradeList/>)]
let tabNames = ["Buildings", "Upgrades"]
let resourcesTab = [(<div><Population/> <ResourcesList/></div>)]
let resourceTabNames = ["Resources"]
let jobTab = [(<JobList/>),(<JobList military={true}/>)]
let jobTabNames = ["Civilian", "Military"]
let actionTab = [(<ActionList/>)]
let actionTabNames = ["Actions"]
export default class MainWindow extends React.Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-2 LightNavy">
                        <TabWindow
                            tabItems={resourcesTab}
                            tabNames={resourceTabNames}
                        />
                    </div>
                    <div className="col-md-2 LightNavy">
                        <TabWindow
                            tabItems={jobTab}
                            tabNames={jobTabNames}
                        />
                    </div>
                    <div className="col-md-2 LightNavy">
                        <TabWindow
                            tabItems = {tabItems}
                            tabNames = {tabNames}
                        />
                    </div>
                    <div className="col-md-2 LightNavy">
                        <TabWindow
                            tabItems={actionTab}
                            tabNames={actionTabNames}
                        />
                    </div>
                    <div className="col-md-4 LightNavy">
                        <TechWindow/>
                    </div>
                    <Log></Log>
                </div>
            </div>
        );
    }
}