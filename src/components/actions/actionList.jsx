import React from 'react';

import BuildingStore from '../../stores/buildingStore.js'
import TechStore from '../../stores/techStore.js'
import Action from './action'
import actions from '../../config/actions.json'

export default class ActionList extends React.Component {
    componentDidMount() {
        BuildingStore.addChangeListener(this.onBuildingChange);
        TechStore.addChangeListener(this.onTechChange);
    }

    onBuildingChange = () => {
        //re-render on tech change to diplay new unlocks
        this.forceUpdate();
    }

    onTechChange = () => {
        //re-render on tech change to diplay new unlocks
        this.forceUpdate();
    }

    createTable = () => {
        let table = Object.keys(actions).map((key) => {
            if (TechStore.isUnlocked(key) || BuildingStore.isUnlocked(key)) {
                return (
                    <div key={key} className='actionDiv'>
                        <Action 
                            action={actions[key]}
                        />
                    </div>
                )
            }
        })
      
        return table
    }

    render() {
        return (
            <div>
                {this.createTable()}
            </div>
        );
    }
}