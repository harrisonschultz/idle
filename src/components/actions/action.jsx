import React from 'react';

import { subtractResource, checkResources, addResource } from '../../actions/resourceAction'
import { addNewPopulation } from '../../actions/populationAction'
import ActionStore from '../../stores/actionStore'
import { addAction } from '../../actions/actionAction'

export default class Action extends React.Component {
    errorResource = '';
    refs = {};

    constructor(props) {
        super(props)
        
        this.state = {
            details: '',
            displayError: false,
            action: {},
            showDetails: false,
        }
    }

    componentDidMount() {
        ActionStore.addSpecificChangeListener(this.onActionChange, this.props.action.name);
        this.onActionChange();
    }

    onActionChange = () => {
        this.setState({action: ActionStore.get(this.props.action.name)});
    }

    addRef = ref => {
        this.refs = ref;
    }

    purchase = () => {
        if (checkResources(this.props.action.cost)) {
            for (var key in this.props.action.cost) {
                if (!subtractResource(key, this.state.action.cost[key])) {
                    this.errorResource = key
                    this.setState({displayError: true})
                    return
                }
            }
            addAction(this.props.action.name);
            this.performAction();
        }
    }

    performAction = () => {
        if(this.props.action.type == 'giftResource') {
            for (var i in this.props.action.awards.resources) {
                addResource(i, this.props.action.awards.resources[i]);
            }
        }
        if(this.props.action.type == 'giftPopulation') {
            addNewPopulation();
        }
    }

    createDetails = (objectToDisplay) => {
        return Object.keys(objectToDisplay).map( resource => {
            return (
                <div key={resource} className="Cost">
                    {resource.replace("_", " ") + ": " + objectToDisplay[resource]}
                </div>
            )
        })
    }
    createAwards = (objectToDisplay) => {
        return Object.keys(objectToDisplay).map(resource => {
            return Object.keys(objectToDisplay[resource]).map(awards => {
                return (
                    <div key={awards} className="Cost">
                        {awards.replace("_", " ") + ": " + objectToDisplay[resource][awards]}
                    </div>
                )
            })
        })
    }

    showError() {  
        //Error displays for 3 seconds
        setTimeout(() => {
            this.setState({displayError: false})
        }, 3000)
        
        return (
            <div className="ActionError">
                {"Not Enough " + this.errorResource}
            </div>
        )
    }

    createToolTip = () => { 
        if (this.state.showDetails) {
            return (
                <div className="BuildingDetails" style={{left: this.refs.offsetLeft + 15, top: this.refs.offsetTop + 30, position: "absolute"}}>
                    <div className='TechTitle'>Cost</div>
                    <div className='TechCost TechPrint'>
                        {this.createDetails(this.state.action.cost)}
                    </div>
                    <hr/>
                    <div className='TechTitle'>Awards</div>
                    <div className='TechAwards TechPrint' >
                        {this.createAwards(this.props.action.awards)}
                    </div>
                </div>
            )
        } else {
            return ""
        }
    }

    hideDetails = () => {
        this.setState({showDetails: false})
    }

    showDetails = () => {
        this.setState({showDetails: true})
    }
    
    render() {
        return (
            <div className='BuildingCell'>
            {this.state.displayError && this.showError()}
                <div 
                    className="Building Clickable" 
                    onClick={this.purchase} 
                    onMouseEnter={this.showDetails}
                    onMouseLeave={this.hideDetails}
                    ref={this.addRef}
                    >
                    {this.props.action.name.replace("_", " ")}
                    {this.createToolTip()}
                </div>
                <div>
                    {this.state.details}
                </div>
            </div>
        );
    }
}