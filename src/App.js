import React, { Component } from 'react';
import './App.css';
import MainWindow from './components/mainWindow';
import Engine from './engine/engine'

class App extends Component {
    engine = new Engine();

    render() {
        return (
            <div className="App container-fluid DarkNavy">
                <MainWindow/>
            </div>
        );
    }
}

export default App;
